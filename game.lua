local game = {}
-- Images
local unicorn
local hill
local rainbow
local background
local window
local num_font
local gameover

-- Sounds
local applause
local flap
local pickup

-- Gobal Variables
local x
local y
local vy
local air_jumps
local jump_cleared
local mirror
local token_count
local sin_time
local gen_current
local gen_cell
local token_data
local game_timer
local message

-- Global constansts
local game_timeout = 60
local token_location = {
   { 
      { x = 3, y = 0 },
      { x = 0, y = 2 } 
   },
   { 
      { x = 2, y = 1 } 
   },
   {
      { x = 3, y = 1 },
      { x = 0, y = 1 },
      { x = 0, y = 6 },
   },
   {
      { x = 3, y = 4 },
      { x = 1, y = 4 },
      { x = 1, y = 4 },
      { x = 1, y = 4 },
      { x = 1, y = 4 },
   },
   {
      { x = 2, y = 1 },
      { x = 0, y = 5 },
      { x = 0, y = 10 },
   },
   {
      { x = 3, y = 1 },
      { x = 1, y = 1 },
      { x = 1, y = 1 },
      { x = 1, y = 1 },
      { x = 1, y = 1 },
   }
}

function generateTokensTo()
   local width, height
   local genTo
   local randindex
   local index
   local i, v
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()

   genTo = x + width * 2

   while gen_current < genTo do
      randindex = love.math.random(1, #token_location)
      for i, v in ipairs(token_location[randindex]) do
         gen_current = gen_current + (v.x * 80)         
         table.insert(token_data, { x = gen_cell + v.x, y = v.y } );
         gen_cell = gen_cell + v.x
      end
      gen_cell = gen_cell + 1
      gen_current = gen_current + 80
   end
end

function drawBackground()
   local gx, gy
   local x_offset
   local width, height;
   local half_width
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   half_width = width / 2
   if x < half_width then
      x_offset = 0
   else
      x_offset = (x - half_width) % 64
   end
   for gy = 0, height,64 do
      for gx = 0, width + 64, 64 do
         love.graphics.draw(background, gx - x_offset, gy)
      end
   end
end

function drawGround()
   local gx
   local width, height
   local x_offset
   local half_width
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   
   half_width = width / 2
   if x < half_width then
      x_offset = 0
   else
      x_offset = (x - half_width) % 140
   end

   for gx = 0, width + 140, 140 do
      love.graphics.draw(hill, gx - x_offset, height - 50)
   end
end

function detectColect()
   local player = {}
   local token = {}
   local width, height
   local v, i
   local remove_list = {}
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   
   player.w = unicorn:getWidth()  - 40
   player.h = unicorn:getHeight() - 40
   token.w = rainbow:getWidth()
   token.h = rainbow:getHeight()

   player.x = x + 20
   player.y = y + 20
   for i, v in ipairs(token_data) do
      token.x = v.x * 80 
      token.y = height - (90 + v.y * 50) 
      if token.x           < player.x + player.w and
         token.x + token.w > player.x and
         token.y           < player.y + player.h and
         token.y + token.h > player.y then
         table.insert(remove_list, i)
         token_count = token_count + 1
      end
   end
   for i = #remove_list, 1, -1 do
      table.remove(token_data, remove_list[i]);
   end

   if #remove_list > 0 then
      love.audio.stop(pickup)
      love.audio.play(pickup)
   end
end

function drawPlayer()
   local player_x
   local local_x
   local width, height
   local half_width
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   half_width = width / 2
   if x < half_width then
      local_x = x
   else
      local_x = half_width
   end

   if mirror == 1 then
      player_x = local_x
   else
      player_x = local_x + unicorn:getWidth() 
   end
   love.graphics.draw(unicorn, player_x, y, 0, mirror, 1)
end

function drawTokens()
   local player_x
   local sin_offset
   local width, height
   local half_width
   local v, i
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   half_width = width / 2

   if x < half_width then
      x_offset = 0
   else
      x_offset = (x - half_width)
   end
   sin_offset = math.sin(sin_time * 4) * 10
   for i, v in ipairs(token_data) do
      love.graphics.draw(rainbow, v.x * 80 - x_offset, height - (90 + v.y * 50) + sin_offset)
   end
end

function game.load()
   unicorn = love.graphics.newImage("assets/Unicorn.png")
   hill = love.graphics.newImage("assets/Hill.png")
   rainbow = love.graphics.newImage("assets/RainbowSmall.png")
   background = love.graphics.newImage("assets/background.png")
   window = love.graphics.newImage("assets/Window.png")
   gameover = love.graphics.newImage("assets/GameOver.png")
   num_font = love.graphics.newImageFont("assets/Numbers.png", 
                                         " 0123456789", 0)

   applause = love.audio.newSource("assets/applause.wav", "static")
   flap = love.audio.newSource("assets/flap.wav", "static")
   pickup = love.audio.newSource("assets/pickup.wav", "static")
end

function game.reset()
   x = 10
   y = 300
   vy = 0
   air_jumps = 0
   jump_cleared = true
   mirror = 1
   token_count = 0
   sin_time = 0
   gen_current = 0
   gen_cell = 0
   game_timer = game_timeout
   message = "none"
   
   token_data = {}
   love.math.setRandomSeed(555)
   generateTokensTo()

   love.graphics.setFont(num_font)
end

function game.update(dt)
   local width, height
   local speed = 400    -- Picked what felt good
   local grav_acc = 400 -- Picked what felt good

   if message == "none" then
      width = love.graphics.getWidth()
      height = love.graphics.getHeight()

      if love.keyboard.isDown("left") then
         x = x - speed * dt
         mirror = -1
      elseif love.keyboard.isDown("right") then
         x = x + speed * dt
         mirror = 1
      end

      if love.keyboard.isDown("up") and air_jumps > 0 and jump_cleared then
         vy = -400
         love.audio.stop(flap)
         love.audio.play(flap)
         air_jumps = air_jumps - 1
         jump_cleared = false
         if air_jumps < 0 then
            air_jumps = 0
         end
      elseif not love.keyboard.isDown("up") then
         jump_cleared = true
      end

      -- Compute Y acceleration
      vy = vy + grav_acc * dt
      y = y + vy * dt

      if y > (height - 150) then
         y = height - 150
         air_jumps = 3
      end

      if x < 10 then
         x = 10
      end

      sin_time = sin_time + dt
      if sin_time > math.pi then
         sin_time = sin_time - math.pi
      end
      detectColect()
      generateTokensTo()
      game_timer = game_timer - dt
      if game_timer <= 0 then
         message = "score"
         love.audio.stop(applause)
         love.audio.play(applause)
      end
   else
      if love.keyboard.isDown("escape") or
         love.keyboard.isDown("return") then
         message = "menu"
      end
   end

  
end

function game.draw()
   local width, height
   width = love.graphics.getWidth()
   height = love.graphics.getHeight()
   love.graphics.setColor(1, 1, 1, 1)
   drawBackground()
   drawGround()
   drawTokens()
   drawPlayer()
   love.graphics.draw(window, 0, 0)
   love.graphics.setColor(0, 0, 0, 1)

   local text_time    = "" .. math.ceil(game_timer)
   local text_timeout = "" .. math.ceil(game_timeout)
   local text_token   = "" .. token_count
   love.graphics.print(text_time, 2, 2)
   love.graphics.print(text_token, 2, 72)
   if message == "score" then
      local gox = width  / 2 - gameover:getWidth()  / 2
      local goy = height / 2 - gameover:getHeight() / 2
      love.graphics.setColor(1, 1, 1, 1)
      love.graphics.draw(gameover, gox, goy)
      love.graphics.setColor(0, 0, 0, 1)
      love.graphics.print(text_timeout,  gox + 142, goy + 210)
      love.graphics.print(text_token, gox + 142, goy + 280)
   end
end

function game.getMessage()
   return { message = message, count = token_count }
end

return game

