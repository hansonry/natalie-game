#!/bin/bash

ROOT="public"
HTTP="."
OUTPUT="index.html" 

i=0
echo "<HTML>" > $OUTPUT
echo "<HEAD><TITLE>Natalie Game Files</TITLE></HEAD>" >> $OUTPUT
echo "<BODY>" >> $OUTPUT
echo "<H1>Natalie Game Files</H1>" >> $OUTPUT
echo "<UL>" >> $OUTPUT
for filepath in `find "$ROOT" -maxdepth 1 -mindepth 1 -type d| sort`; do
  path=`basename "$filepath"`
  echo "  <LI>$path</LI>" >> $OUTPUT
  echo "  <UL>" >> $OUTPUT
  for i in `find "$filepath" -maxdepth 1 -mindepth 1 -type f| sort`; do
    file=`basename "$i"`
    echo "    <LI><a href=\"$HTTP/$path/$file\">$file</a></LI>" >> $OUTPUT
  done
  echo "  </UL>" >> $OUTPUT
done
for i in `find "$ROOT" -maxdepth 1 -mindepth 1 -type f| sort`; do
 file=`basename "$i"`
 echo "    <LI><a href=\"$HTTP/$file\">$file</a></LI>" >> $OUTPUT
done
echo "</UL>" >> $OUTPUT
echo "</BODY>" >> $OUTPUT
echo "</HTML>" >> $OUTPUT
