local menu = {}

local fade_timeout = 1.5
local fade_timer

local selector_index
local selector_position = {
   { x = 299, y = 240, name = "play" },
   { x = 299, y = 319, name = "exit" }
}
local message

-- Images
local background
local rainbow

-- Sounds
local pick
local accept



function menu.load()
   rainbow = love.graphics.newImage("assets/RainbowSmall.png")
   background = love.graphics.newImage("assets/MainMenu.png")
   pick = love.audio.newSource("assets/MENU_Pick.wav", "static")
   accept = love.audio.newSource("assets/MENU_Accept.wav", "static")
end

function menu.reset()
   selector_index = 1
   message = "none"
   fade_timer = fade_timeout
end

function menu.update(dt)
   if message == "fade" then
      fade_timer = fade_timer - dt
   end
   if fade_timer < 0 then
      message = "play"
   end
end

function menu.draw()
   local fade
   local rpos = selector_position[selector_index]
   fade = fade_timer / fade_timeout
   love.graphics.setColor(fade, fade, fade, 1)
   love.graphics.draw(background, 0, 0)
   love.graphics.draw(rainbow, rpos.x, rpos.y)
end

function menu.keypressed(key)
   if message ~= "none" then
      return
   end
   local index = selector_index
   if key == "down" then
      index = index + 1
   elseif key == "up" then
      index = index - 1
   end

   if index > 2 then
      index = 2
   elseif index < 1 then
      index = 1
   end

   if index ~= selector_index then
      selector_index = index
      love.audio.stop(pick)
      love.audio.play(pick)
   end

   local rpos = selector_position[selector_index]
   if key == "return" then
      if rpos.name == "play" then
         love.audio.stop(accept)
         love.audio.play(accept)
         message = "fade"
      elseif rpos.name == "exit" then
         message = "exit"
      end
   end
   if key == "escape" then
      message = "exit"
   end
end

function menu.getMessage()
   return { message = message }
end

return menu

