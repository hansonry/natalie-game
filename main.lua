local statemods = { 
   game = require("game"),
   menu = require("menu")
}
local state

function love.load()
   local k, v
   love.window.setTitle("Natalie's Game")
   state = "menu"
   for k, v in pairs(statemods) do
      v.load()
   end
   statemods[state].reset()
   print("Resolution: ", love.graphics.getWidth(), love.graphics.getHeight())
end

function love.update(dt)
   local statemod = statemods[state]
   statemod.update(dt)
   local message = statemod.getMessage()
   local msg = message.message
   if msg == "exit" then
      love.event.quit()
   elseif msg == "play" then
      state = "game"
      statemods.game.reset()
   elseif msg == "menu" then
      state = "menu"
      statemods.menu.reset()
   end
end


function love.draw()
   statemods[state].draw()
end

function love.keypressed(key)
   local statemod = statemods[state]
   if statemod.keypressed ~= nil then
      statemod.keypressed(key)
   end
end

function love.keyreleased(key)
end

